// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __POWERUPMANAGER__H__
#define __POWERUPMANAGER__H__

#include "movement.h"
#include "sprites.h"

class Player;
class PowerUp : public Character {
public:
    PowerUp();
    virtual ~PowerUp();
    void resetPos();
    virtual void update( double dt );
    virtual void draw();
    virtual void activate( Player *who );
    virtual void disable();
    virtual void extraUpdate(double dt);
    virtual const char*description() = 0;

protected:
    Player *owner;
    bool active;
    double activeTimer;
    double activeDelay;
};

class PowerUpManagerPrivate;
class PowerUpManager {
public:
    PowerUpManager();
    ~PowerUpManager();
    void load();
    void update( double dt );
    void draw();


    void spawn();
    bool update_spawnTimer(double dt);
    bool check_shot(const Box &b);
    PowerUp *last_collided();
    void adjustColumnWidth();
private:
    PowerUpManagerPrivate *d;
};

#endif
