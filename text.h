// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TEXT__H__
#define __TEXT__H__

// for throwaway texts, there is a "print_nocache" method
// that skips the caching, so that memory doesn't get bloated

#include "sprites.h"
//class Color;

typedef int TextHandle;

class Text {
public:
    Text(const char *_str, const Color &col = Color(0,0,0)) {}
    virtual ~Text() {}

    virtual void draw(int x, int y) = 0;
    virtual void colorize(const Color &col) = 0;
    virtual const char *str() const = 0;
};

class TextManager {
public:
    TextManager();
    ~TextManager();

    void setFont(const char *fontPath, int pointSize);
    void setColor(const Color &col);
    TextHandle print(const char *_str, int x, int y);
    TextHandle print(const TextHandle textHandle, int x, int y);
    void print_nocache(const char *_str, int x, int y);
    void discard(const TextHandle textHandle);

    static void print_once(const char *_str, int x, int y, const Color &col, const char *_fontName, int fontSize);

private:
    class TextManagerPrivate;
    TextManagerPrivate *d;
};

#endif
