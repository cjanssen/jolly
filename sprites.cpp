// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "sprites.h"
#include "math.h"
#include "mainloop.h"

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
//#include "SDL/SDL_gfxBlitFunc.h"

// NOTE: look at physfs!

#define __DRAW_CENTERED__

const SDL_Color Color::toSDL() const
{
    return {r,g,b};
}

Image::Image(const char *_filename) : data(0)
{
    filename[0] = 0;
    load(_filename);
}

Image::~Image()
{
    if (data) SDL_FreeSurface(data);
}

int Image::width() const
{
    if (!data)
        return 0;

    return data->w;
}

int Image::height() const
{
    if (!data)
        return 0;

    return data->h;
}

int Image::length() const
{
    if (!data)
        return 0;
    return 1;
}

void Image::load(const char *_filename) {
    if (data)
        SDL_FreeSurface(data);

    if (_filename)
        strncpy(filename, _filename, 256);

    if (filename[0]==0)
        return;

    SDL_Surface *tmpSurface = IMG_Load(filename);
    if (!tmpSurface)
        throw "Could not load file!";

    data = SDL_DisplayFormatAlpha(tmpSurface);
    SDL_FreeSurface(tmpSurface);
}

void Image::draw(int x,int y, SDL_Surface *destSurf, bool centered)
{
    if (!data) {
        printf("Trying to draw empty image (%s)\n", filename);
        return;
    }

    if (!destSurf)
        destSurf = MainLoop::screenHandle();

    SDL_Rect centeredRect = {x - data->w/2, y - data->h/2, data->w, data->h};
    SDL_Rect origRect = {x, y, data->w, data->h};
    SDL_Rect *destRect;
    if (centered)
        destRect = &centeredRect;
    else
        destRect = &origRect;

    SDL_BlitSurface(data, NULL, destSurf, destRect);
}

void Image::section(const char *_filename, int x, int y, int w, int h)
{
    if (_filename)
        load(_filename);

    if (!data)
        return;

    const SDL_PixelFormat fmt = *(data->format);
    SDL_Surface *destSurf = SDL_CreateRGBSurface(data->flags, w, h, fmt.BitsPerPixel, fmt.Rmask, fmt.Gmask, fmt.Bmask, fmt.Amask);

    SDL_Rect sourceRect = {x, y, w, h};
    SDL_SetAlpha(data, 0, 255); // overwrite the alpha channel, don't blend
    SDL_BlitSurface(data, &sourceRect, destSurf, NULL);

    // release the cached image
    SDL_FreeSurface( data );
    data = destSurf;
}

void Image::section(const char *_filename, double x_fraction, double y_fraction, double w_fraction, double h_fraction)
{
    if (_filename)
        load(_filename);

    if (!data)
        return;

    int x = x_fraction * data->w;
    int y = y_fraction * data->h;
    int w = w_fraction * data->w;
    int h = h_fraction * data->h;
    section(NULL, x, y, w, h);
}

void Image::section(const Image &refImage, int x, int y, int w, int h)
{
    if (!refImage.data)
        return;

    if (data)
        SDL_FreeSurface(data);

    data = SDL_CreateRGBSurface(refImage.data->flags, w, h,
                                                 refImage.data->format->BitsPerPixel,
                                                 refImage.data->format->Rmask,
                                                 refImage.data->format->Gmask,
                                                 refImage.data->format->Bmask,
                                                 refImage.data->format->Amask);

    SDL_Rect sourceRect = {x, y, w, h};
    SDL_SetAlpha(refImage.data, 0, data->format->alpha);
    SDL_BlitSurface(refImage.data, &sourceRect, data, 0);
    SDL_SetAlpha(refImage.data, SDL_SRCALPHA, data->format->alpha);
}

//*****************************************

Animation::Animation() : strip(0)
{}
Animation::~Animation()
{
    delete [] strip;
}

Animation::Animation(const char *filename, int _w, int _h, double _frametime, bool _looping )
{
    Image _strip(filename);
    init(_strip, _w, _h, _frametime, _looping);
}
Animation::Animation(const Image &_strip, int _w, int _h, double _frametime, bool _looping )
{
    init(_strip, _w, _h, _frametime, _looping );
}

int Animation::width() const
{
    if (!strip)
        return 0;
    return frame_w;
}

int Animation::height() const
{
    if (!strip)
        return 0;
    return frame_h;
}

int Animation::length() const
{
    if (!strip)
        return 0;
    return frame_count;
}

void Animation::init(const Image &_strip, int _w, int _h, double _frametime, bool _looping )
{
    frame_w = _w;
    frame_h = _h;
    frame_count = _strip.width() / _w;
    frame_ndx = 0;
    frame_delay = _frametime;
    frame_timer = _frametime;
    looping = _looping;

    strip = new Image[frame_count];
    for (int i=0; i < frame_count; i++) {
        strip[i].section(_strip, frame_w * i, 0, frame_w, frame_h);
    }
}


void Animation::reset( )
{
    frame_timer = frame_delay;
    frame_ndx = 0;
}

void Animation::update( double dt )
{
    if (!looping && frame_ndx == frame_count-1)
        return;

    frame_timer -= dt;
    while (frame_timer <= 0) {
        frame_timer += frame_delay;
        frame_ndx = ++frame_ndx % frame_count;
        if (!looping && frame_ndx == frame_count-1)
            break;
    }
}

void Animation::forceFrame(int n)
{
    frame_ndx = n;
}

void Animation::draw(int x, int y, SDL_Surface *destSurf, bool centered)
{
    strip[frame_ndx].draw(x,y, destSurf, centered);
}

//******************************************

// ToDo: this should be something else, a struct?
Color Colors::green = Color(124,164,26);
Color Colors::dk_green = Color(120,115,19);
Color Colors::red = Color(164,26,33);
Color Colors::black = Color(33,18,5);
Color Colors::orange = Color(206,111,33);
Color Colors::dk_red = Color(120,19,24);
Color Colors::lt_red = Color(183,29,39);
Color Colors::dark_black = Color(0,0,0);
Color Colors::lt_blue = Color(26,122,160);
Color Colors::yellow = Color(207,198,33);
Color Colors::background = Colors::orange;
Color Colors::foreground = Colors::dk_red;

AnimationInstance::AnimationInstance(Animation *animation)
{
    referenceAnim = animation;
    if (animation)
        reset();
}

AnimationInstance::~AnimationInstance()
{
    referenceAnim = 0;
}

void AnimationInstance::fromAnimation(Animation *animation)
{
    referenceAnim = animation;
    if (animation)
        reset();
}

int AnimationInstance::width() const
{
    return referenceAnim->width();
}

int AnimationInstance::height() const
{
    return referenceAnim->height();
}

int AnimationInstance::length() const
{
    return referenceAnim->length();
}


void AnimationInstance::reset()
{
    frame_timer = referenceAnim->frame_delay;
    frame_ndx = 0;
}

void AnimationInstance::update(double dt)
{
    if (!referenceAnim->looping && frame_ndx == referenceAnim->frame_count-1)
        return;

    frame_timer -= dt;
    while (frame_timer <= 0) {
        frame_timer += referenceAnim->frame_delay;
        frame_ndx = ++frame_ndx % referenceAnim->frame_count;
        if (!referenceAnim->looping && frame_ndx == referenceAnim->frame_count-1)
            break;
    }
}

void AnimationInstance::draw(int x, int y, SDL_Surface *destSurf, bool centered)
{
    referenceAnim->forceFrame(frame_ndx);
    referenceAnim->draw(x,y,destSurf,centered);
//    referenceAnim->strip[frame_ndx].draw(x,y, destSurf, centered);
}

void AnimationInstance::forceFrame(int n)
{
    frame_ndx = n;
}

