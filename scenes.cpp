// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "scenes.h"

SceneManager::SceneManager()
{}

SceneManager::~SceneManager()
{}

int SceneManager::append(State *newState)
{
    // make sure that we only append scenes
    if (dynamic_cast<Scene *>(newState) == 0)
        return -1;
    StateManager::append(newState);
}

Scene *SceneManager::currentScene() const
{
    return dynamic_cast<Scene *>(currentState());
}

void SceneManager::update(double dt)
{
    if (validState())
        currentScene()->update(dt);
}

void SceneManager::draw()
{
    if (validState())
        currentScene()->draw();
}
