// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BULLETS__H__
#define __BULLETS__H__

#include "movement.h"

class BulletPrivate;
class Player;
class PowerUpManager;

class Bullet : public Character {
public:
    Bullet(Vector _pos,
           Vector _dir,
           double _speed,
           double _damage,
           Drawable *bulletpic,
           Player *owner,
           Player *enemy,
           int warps);
    ~Bullet();
    void update(double dt);
    void draw();

public:
    BulletPrivate *d;
};

#endif
