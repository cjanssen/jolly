// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "text.h"

// for the color only
#include "sprites.h"
#include "mainloop.h"

#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"

#include <vector>
#include <ext/hash_map>


// NOTE: this is for linux
namespace std { using namespace __gnu_cxx; }

//////////////////////////////////////

class TextImpl : public Text {
public:
    TextImpl(const char *_str, TTF_Font *font, const Color &col = Color(0,0,0));
    ~TextImpl();

    virtual void draw(int x, int y);
    virtual void colorize(const Color &col);
    const char *str() const;

private:
    void copyString(const char *_str);
    void render();

private:
    Color currentColor;
    char *currentString;
    TTF_Font *currentFont;
    SDL_Surface *surface;

};

TextImpl::TextImpl(const char *_str, TTF_Font *font, const Color &col)
    : Text(_str, col), surface(0), currentString(0), currentColor(col), currentFont(font)
{
    copyString(_str);
    render();
}

TextImpl::~TextImpl()
{
    delete [] currentString;
    if (surface)
        SDL_FreeSurface(surface);
}

const char *TextImpl::str() const
{
    return currentString;
}

void TextImpl::copyString(const char *_str)
{
    if (!_str || strlen(_str)==0)
        return;
    currentString = new char[strlen(_str)+1];
    strcpy(currentString, _str);
}

void TextImpl::render()
{
    if (!currentString || currentString[0]==0)
        return;

    if (!currentFont) {
        printf("No font loaded when printing \"%s\"\n", currentString);
        return;
    }
    if (surface) {
        SDL_FreeSurface(surface);
    }

    SDL_Color textColor = currentColor.toSDL();
    surface = TTF_RenderText_Blended(currentFont, currentString, textColor);
    if (!surface) {
        printf("Error when printing \"%s\" : %s\n", currentString, SDL_GetError());
        return;
    }
}

void TextImpl::draw(int x, int y)
{
    if (!surface)
        return;

    if (x < 0)
        x += MainLoop::screenWidth() - surface->w;
    if (y < 0)
        y += MainLoop::screenHeight() - surface->h;

    SDL_Rect r = {x, y, surface->w, surface->h};
    SDL_BlitSurface(surface, NULL, MainLoop::screenHandle(), &r);
}

void TextImpl::colorize(const Color &col)
{
    if (currentColor != col) {
        currentColor = col;
        render();
    }
}

/////////////////////////////////////////////////////////

class TextManager::TextManagerPrivate {
public:
    TextManagerPrivate(TextManager *qq):q(qq) {}
    ~TextManagerPrivate() {}
    TextManager *q;

    TTF_Font *font;
    Color color;
    std::vector<Text *> textCache;
    std::hash_map<const char *, int> textHash;
};


TextManager::TextManager():d(new TextManagerPrivate(this))
{
    d->font = 0;
    d->color = Color(0,0,0);
}

TextManager::~TextManager()
{
    if (d->font)
        TTF_CloseFont(d->font);


    // delete all cached texts
    for (int i = 0; i < d->textCache.size(); i++)
        delete d->textCache[i];

    d->textCache.clear();
    d->textHash.clear();

    delete d;
}

void TextManager::setFont(const char *fontPath, int pointSize)
{
    if (d->font)
        TTF_CloseFont(d->font);

    d->font = TTF_OpenFont(fontPath, pointSize);
}

void TextManager::setColor(const Color &col)
{
    d->color = col;
}

TextHandle TextManager::print(const char *_str, int x, int y)
{
    Text *newText = 0;
    int handle = d->textHash[_str] - 1;
    if (handle != -1) {
        newText = d->textCache[handle];
        newText->colorize(d->color);
    } else {
        newText = new TextImpl(_str, d->font, d->color);
        d->textCache.push_back(newText);
        handle = d->textCache.size() - 1;
        d->textHash[_str] = handle + 1;
    }

    newText->draw(x, y);

    // return the position in the cache
    return handle;
}

TextHandle TextManager::print(const TextHandle textHandle, int x, int y)
{
    // fetch cached text
    Text *fetchedText = d->textCache.at(textHandle);

    // colorize
    fetchedText->colorize(d->color);

    // print it
    fetchedText->draw(x, y);

    // return the input param
    return textHandle;
}

void TextManager::print_nocache(const char *_str, int x, int y)
{
    TextImpl newText(_str, d->font, d->color);
    newText.draw(x, y);
}

void TextManager::discard(const TextHandle textHandle)
{
    const char *str = d->textCache[textHandle]->str();
    d->textHash.erase(str);
    d->textCache.erase(d->textCache.begin() + textHandle);
}

void TextManager::print_once(const char *_str, int x, int y, const Color &col, const char *_fontName, int fontSize)
{
    TTF_Font *once_font = TTF_OpenFont(_fontName, fontSize);
    if (!once_font)
        return;

    TextImpl newText(_str, once_font, col);
    newText.draw(x, y);

    TTF_CloseFont(once_font);
}

