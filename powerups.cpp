// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "powerups.h"
#include "player.h"
#include "imageresources.h"

#include <stdio.h>
#include <stdlib.h>


PowerUp *newPowerUp()
{
    switch(random() % 9) {
    case 0: return new PU_Doubleshotfreq;
    case 1: return new PU_Halfshotfreq;
    case 2: return new PU_Doubleshotspeed;
    case 3: return new PU_Halfshotspeed;
    //case 4: return new PU_Biggershot;
    //case 5: return new PU_Smallershot;
    case 4: return new PU_StrongerGravity;
    case 5: return new PU_WeakerGravity;
//    case 6: return new PU_InvertGravity;
    case 6: return new PU_MultiShot;
    case 7: return new PU_BulletWarp;
    case 8: return new PU_BackwardShot;
    }
    return 0;
}

//******************************************************

PU_Doubleshotfreq::PU_Doubleshotfreq()
{
    currentImage = &(ImageSpace::instance->powerup1);
    resetPos();
}

PU_Doubleshotfreq::~PU_Doubleshotfreq()
{

}

void PU_Doubleshotfreq::activate(Player *who)
{
    PowerUp::activate(who);
    //owner->shoot_delay *= 0.5;
    owner->shoot_timer.setDuration(owner->shoot_timer.duration() * 0.5);
}

void PU_Doubleshotfreq::disable()
{
//    owner->shoot_delay /= 0.5;
    owner->shoot_timer.setDuration(owner->shoot_timer.duration() / 0.5);

    PowerUp::disable();
}

const char *PU_Doubleshotfreq::description()
{
    return "double shooting frequency";
}

//*******************************************
PU_Halfshotfreq::PU_Halfshotfreq()
{
    currentImage = &(ImageSpace::instance->powerup1);
    resetPos();
}

PU_Halfshotfreq::~PU_Halfshotfreq()
{

}

void PU_Halfshotfreq::activate(Player *who)
{
    PowerUp::activate(who);
//    owner->shoot_delay /= 0.5;
    owner->shoot_timer.setDuration(owner->shoot_timer.duration() / 0.5);
}

void PU_Halfshotfreq::disable()
{
//    owner->shoot_delay *= 0.5;
    owner->shoot_timer.setDuration(owner->shoot_timer.duration() * 0.5);

    PowerUp::disable();
}

const char *PU_Halfshotfreq::description()
{
    return "half shooting frequency";
}

//*******************************************
PU_Doubleshotspeed::PU_Doubleshotspeed()
{
    currentImage = &(ImageSpace::instance->powerup1);
    resetPos();
}

PU_Doubleshotspeed::~PU_Doubleshotspeed()
{
}

void PU_Doubleshotspeed::activate(Player *who)
{
    PowerUp::activate(who);
    owner->bulletspeed /= 0.5;
}

void PU_Doubleshotspeed::disable()
{
    owner->bulletspeed *= 0.5;

    PowerUp::disable();
}

const char *PU_Doubleshotspeed::description()
{
    return "double shooting speed";
}

//*******************************************
PU_Halfshotspeed::PU_Halfshotspeed()
{
    currentImage = &(ImageSpace::instance->powerup1);
    resetPos();
}

PU_Halfshotspeed::~PU_Halfshotspeed()
{
}

void PU_Halfshotspeed::activate(Player *who)
{
    PowerUp::activate(who);
    owner->bulletspeed *= 0.5;
}

void PU_Halfshotspeed::disable()
{
    owner->bulletspeed /= 0.5;

    PowerUp::disable();
}

const char *PU_Halfshotspeed::description()
{
    return "half shooting speed";
}

//*******************************************
#if 0
PU_Biggershot::PU_Biggershot()
{
    currentImage = &(ImageSpace::instance->powerup2);
    resetPos();
}

PU_Biggershot::~PU_Biggershot()
{
}

void PU_Biggershot::activate(Player *who)
{
    PowerUp::activate(who);
    owner->bulletsize++;
    if (owner->bulletsize>3)
        owner->bulletsize = 3;
}

void PU_Biggershot::disable()
{
    owner->bulletsize--;
    if (owner->bulletsize<0)
        owner->bulletsize = 0;
    PowerUp::disable();
}


//*******************************************
PU_Smallershot::PU_Smallershot()
{
    currentImage = &(ImageSpace::instance->powerup2);
    resetPos();
}

PU_Smallershot::~PU_Smallershot()
{
}

void PU_Smallershot::activate(Player *who)
{
    PowerUp::activate(who);
    owner->bulletsize--;
    if (owner->bulletsize<0)
        owner->bulletsize = 0;
}

void PU_Smallershot::disable()
{
    owner->bulletsize++;
    if (owner->bulletsize>3)
        owner->bulletsize = 3;

    PowerUp::disable();
}
#endif
//*******************************************
PU_StrongerGravity::PU_StrongerGravity()
{
    currentImage = &(ImageSpace::instance->powerup4);
    resetPos();
}

PU_StrongerGravity::~PU_StrongerGravity()
{
}

void PU_StrongerGravity::activate(Player *who)
{
    PowerUp::activate(who);
    owner->acceleration /= 0.5;
    owner->gravityAccel /= 0.5;
}

void PU_StrongerGravity::disable()
{
    owner->acceleration *= 0.5;
    owner->gravityAccel *= 0.5;

    PowerUp::disable();
}

const char *PU_StrongerGravity::description()
{
    return "stronger gravity";
}

//*******************************************
PU_WeakerGravity::PU_WeakerGravity()
{
    currentImage = &(ImageSpace::instance->powerup4);
    resetPos();
}

PU_WeakerGravity::~PU_WeakerGravity()
{
}

void PU_WeakerGravity::activate(Player *who)
{
    PowerUp::activate(who);
    owner->acceleration *= 0.5;
    owner->gravityAccel *= 0.5;
}

void PU_WeakerGravity::disable()
{
    owner->acceleration /= 0.5;
    owner->gravityAccel /= 0.5;

    PowerUp::disable();
}

const char *PU_WeakerGravity::description()
{
    return "weaker gravity";
}

//*******************************************
PU_InvertGravity::PU_InvertGravity()
{
    currentImage = &(ImageSpace::instance->powerup4);
    resetPos();
}

PU_InvertGravity::~PU_InvertGravity()
{
}

void PU_InvertGravity::activate(Player *who)
{
    PowerUp::activate(who);
    owner->inverseGravity = !owner->inverseGravity;
}

void PU_InvertGravity::disable()
{
    owner->inverseGravity = !owner->inverseGravity;

    PowerUp::disable();
}

const char *PU_InvertGravity::description()
{
    return "invert gravity";
}

//*******************************************
PU_MultiShot::PU_MultiShot()
{
    currentImage = &(ImageSpace::instance->powerup2);
    resetPos();
}

PU_MultiShot::~PU_MultiShot()
{
}

void PU_MultiShot::activate(Player *who)
{
    PowerUp::activate(who);
    owner->bulletspershot += 1;
    if (owner->bulletspershot > 5)
        owner->bulletspershot = 5;
}

void PU_MultiShot::disable()
{
    owner->bulletspershot -= 1;
    if (owner->bulletspershot < 1)
        owner->bulletspershot = 1;

    PowerUp::disable();
}

const char *PU_MultiShot::description()
{
    return "multishot";
}

//*******************************************
PU_BulletWarp::PU_BulletWarp()
{
    currentImage = &(ImageSpace::instance->powerup3);
    resetPos();
}

PU_BulletWarp::~PU_BulletWarp()
{
}

void PU_BulletWarp::activate(Player *who)
{
    PowerUp::activate(who);
    owner->bulletWarp++;
}

void PU_BulletWarp::disable()
{
    owner->bulletWarp--;

    PowerUp::disable();
}

const char *PU_BulletWarp::description()
{
    return "screen wrap";
}

//*******************************************
PU_BackwardShot::PU_BackwardShot()
{
    currentImage = &ImageSpace::instance->powerup2;
    resetPos();
}

PU_BackwardShot::~PU_BackwardShot()
{
}

void PU_BackwardShot::activate(Player *who)
{
    PowerUp::activate(who);
    owner->backshot++;
}

void PU_BackwardShot::disable()
{
    owner->backshot--;

    PowerUp::disable();
}

const char *PU_BackwardShot::description()
{
    return "Back shot";
}
