// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MYMATH__H__
#define __MYMATH__H__

#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679

class Matrix {
public:
    Matrix(double _a,double _b, double _c, double _d):a(_a),b(_b),c(_c),d(_d){}
    double operator[](const int ndx) const {
        if (ndx==0) return a;
        if (ndx==1) return b;
        if (ndx==2) return c;
        if (ndx==3) return d;
        throw "OUTOFBOUNDS";
    }
    public:
    double a,b,c,d;
};
class Vector {
public:
    Vector():valid(false){}
    Vector(double _a,double _b):x(_a),y(_b),valid(true){}
    Vector operator*(const Matrix &m) const { return Vector(x*m.a + y*m.b, x*m.c + y*m.d); }
    Vector operator+(const Vector &v) const { return Vector(x+v.x, y+v.y); }
    Vector operator*(const double d) const { return Vector(x*d, y*d); }
    Vector invert() { return Vector(-x,-y); }
    double operator[](const int ndx) const {
        if (ndx==0) return x;
        if (ndx==1) return y;
        throw "OUTOFBOUNDS";
    }
    double x,y;
    bool valid;
};

class Box {
public:
    Box(double _a,double _b, double _c, double _d):x1(_a),y1(_b),x2(_c),y2(_d),valid(true){}
    Box():valid(false){}
    double operator[](const int ndx) const {
        if (ndx==0) return x1;
        if (ndx==1) return y1;
        if (ndx==2) return x2;
        if (ndx==3) return y2;
        throw "OUTOFBOUNDS";
    }
    public:
    double x1,y1,x2,y2;
    bool valid;
};
class Building {
public:
    Building() { count = 0; collision = 0;}
    void pushBox(const Box &box) {
        if (collision) {
            // TODO: use linked lists
            Box *oldlist = collision;
            collision = new Box[count+1];
            for (int i=0; i<count; i++)
                collision[i] = oldlist[i];
            delete oldlist;
        } else {
            collision = new Box[1];
        }
        collision[count++] = box;
    }
    int count;
    Box *collision;
};

typedef Box Segment;

struct Triplet {
    Triplet(double ds,double _x, double _y):distSq(ds),x(_x),y(_y){}
    double distSq;
    double x;
    double y;
};

class mymath {
public:
    mymath() {}
    ~mymath() {}

    static Matrix get_rotation_matrix( double angle );
    static Vector rotate(const Vector &vector, double angle);
    static Vector disturb_vector(const Vector &vector, double deviation);
    static double sign( double a );
    static double round( double r );
    static double min(double a, double b);
    static double max(double a, double b);
    static Vector normalize_vector( const Vector &vec );
    static double get_angle( const Vector &v1, const Vector &v2 );

    static void init_random();
    static double unif_rand();
    static double randn();
    static Vector randn2();
    static void permutation( int *result, int len );
    static double get_distance(const Vector &point1, const Vector &point2);
    static double get_distanceSq(const Vector &point1, const Vector &point2);
    static bool check_intersection(const Box & segment, const Box & box);
    static bool check_boxes(const Box & box1, const Box & box2);
    static bool check_boxinbox(const Box & small_box, const Box & big_box);
    static bool check_pointinbox(const Vector &point, const Box & box);
    static Vector get_dir_vector(double origx, double origy, double destx, double desty);
    static Vector get_dir_vector(const Vector &from, const Vector &to);
    static bool check_pointinbuilding( const Vector &point, const Building & building );
    static bool check_segmentinbuilding( const Box & segment, const Building & building );
    static Triplet distanceSq_to_box( const Vector &point, const Box & box );
    static bool check_boxinbuilding( const Box & box1, const Building & building );

private:
    static bool check_intersection_internal( const Box & segment, const Box & box );
};

#endif
