// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "keys.h"

#include "SDL/SDL.h"

#include <ext/hash_map>
#include <list>

// NOTE: this is for linux
namespace std { using namespace __gnu_cxx; }

typedef std::list<int> intList;
typedef std::list<int>::iterator listIter;

class KeyboardManager::KeysPrivate {
public:
    KeysPrivate(KeyboardManager *qq):q(qq) {}
    KeyboardManager *q;

    std::vector<bool *> actionReference;
    std::vector<intList> keyLookup;
    std::hash_map<int, int> reverseLookup;
    int pressedCount;
};


KeyboardManager::KeyboardManager():d(new KeysPrivate(this))
{
}

KeyboardManager::~KeyboardManager()
{
    for (int i=0; i<count(); i++)
        delete d->actionReference[i];
    delete d;
}

int KeyboardManager::count() const
{
    return d->keyLookup.size();
}

bool KeyboardManager::load()
{
    d->pressedCount = 0;
    return true;
}

void KeyboardManager::reset()
{
    d->pressedCount = 0;
    for (int i = 0; i < count(); i++) {
        *d->actionReference[i] = false;
    }
}

void KeyboardManager::managePressed(const int keyCode)
{
    int action = actionForCode(keyCode);
    if (action == -1)
        return;

    if (!*d->actionReference[action])
        d->pressedCount++;

    *d->actionReference[action] = true;
}

void KeyboardManager::manageReleased(const int keyCode)
{
    int action = actionForCode(keyCode);
    if (action == -1)
        return;

    if (*d->actionReference[action])
        d->pressedCount--;

    *d->actionReference[action] = false;
}

bool KeyboardManager::isPressed(const ActionCode action) const
{
    return *d->actionReference[action];
}

bool KeyboardManager::consumePressed(const ActionCode action)
{
    bool retVal = isPressed(action);
    if (retVal) {
        d->pressedCount--;
        *d->actionReference[action] = false;
    }
    return retVal;
}

bool KeyboardManager::noKey() const
{
    return d->pressedCount == 0;
}

int KeyboardManager::newAction(const int keyCode)
{
    int newIndex = count();
    d->actionReference.push_back(new bool(false));
    d->keyLookup.push_back(intList(1, keyCode));
    d->reverseLookup[keyCode] = newIndex;
    return newIndex;
}

void KeyboardManager::setKey(const ActionCode action, const int keyCode)
{
    if (action < 0 || action >= count())
        return;

    cleanAction(action);
    addKey(action, keyCode);
}

void KeyboardManager::addKey(const ActionCode action, const int keyCode)
{
    if (action < 0 || action >= count())
        return;

    // add one more key to an existing action
    d->keyLookup[action].push_back(keyCode);
    d->reverseLookup[keyCode] = action;
}

void KeyboardManager::removeKey(const int keyCode)
{
    // remove one key from whichever action it is associated
    int ndx = actionForCode(keyCode);
    if (ndx == -1)
        return;
    d->reverseLookup.erase(keyCode);
    for (listIter i = d->keyLookup[ndx].begin(); i != d->keyLookup[ndx].end(); ++i) {
        d->keyLookup[ndx].erase(i);
        break;
    }
}

void KeyboardManager::cleanAction(const ActionCode action)
{
    // remove all keys associated to that action
    if (action < 0 || action >= count())
        return;

    int ndx = action;

    for (listIter i = d->keyLookup[ndx].begin(); i != d->keyLookup[ndx].end(); ++i) {
        d->reverseLookup.erase(*i);
    }
    d->keyLookup[ndx].clear();
}

std::list<int> KeyboardManager::codeForAction(const ActionCode action) const
{
    if (action < 0 || action > count())
        return intList();
    return d->keyLookup[action];
}

KeyboardManager::ActionCode KeyboardManager::actionForCode(const int keyCode) const
{
    std::hash_map <int,int>::const_iterator pos = d->reverseLookup.find(keyCode);
    if (pos != d->reverseLookup.end())
        return KeyboardManager::ActionCode(pos->second);
    else
        return KeyboardManager::ActionCode(-1);
}

bool *KeyboardManager::actionBinding(const KeyboardManager::ActionCode action) const
{
    if (action < 0 || action > count())
        return 0;
    return d->actionReference[action];
}
