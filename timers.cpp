// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "timers.h"

#include <stdio.h>

Timer::Timer(double duration)
{
    tmax = duration;
    t = 0;
    repeating = false;
    active = true;
}

Timer::~Timer()
{
}

void Timer::update(double dt)
{
    if (!active)
        return;

    trigger = false;
    if (t > 0) {
        t -= dt;
        if (t<=0) {
            trigger = true;
            if (repeating)
                t = tmax;
        }
    } else if (repeating && tmax > 0) {
        t = tmax;
        update(dt);
    }
}

bool Timer::triggered()
{
    return trigger;
}

void Timer::reset(double initialTime)
{
    if (initialTime > 0)
        t = initialTime;
    else
        t = tmax;
}

double Timer::fraction()
{
    double reftime = elapsed()/tmax;
    if (reftime > 1.0)
        return 1.0;
    return reftime;
}

double Timer::elapsed()
{
    if (t <= tmax)
        return (tmax-t);
    return 0.0;
}

void Timer::setRepeating(bool r)
{
    repeating = r;
}

void Timer::setDuration(double d)
{
    tmax = d;
}

double Timer::duration()
{
    return tmax;
}

void Timer::setActive(bool on)
{
    active = on;
}
