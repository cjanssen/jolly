// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "movement.h"
#include "mymath.h"
#include "mainloop.h"
#include <stdio.h>

class Character;

Character::Character()
{
    currentImage = 0;
    canexitScreen = false;
    alive = true;
    visible = true;
}

Character::~Character()
{}

void Character::update(double dt)
{
    // just for convenience, move to the upperleft corner now, and back to the center at the }
    if (dir.x==0 && dir.y==0) {
        return;
    }

    Vector newpos = pos + dir * dt * speed;

    if (!canexitScreen) {
        int hl,hr,hu,hd;
        hl = mymath::round(spritesize.x/2-collisionbox.x1);
        hr = mymath::round(spritesize.x/2-(spritesize.x-collisionbox.x2));
        hu = mymath::round(spritesize.y/2-collisionbox.y1);
        hd = mymath::round(spritesize.y/2-(spritesize.y-collisionbox.y2));

        if (newpos.x<=hl) {
            newpos.x= hl;
            dir.x=0;
        }

        if (newpos.x>=MainLoop::screenWidth()-hr) {
            newpos.x=MainLoop::screenWidth()-hr;
            dir.x=0;
        }

        if (newpos.y<=hu) {
            newpos.y=hu;
            dir.y=0;
        }

        if (newpos.y>=MainLoop::screenHeight()-hd) {
            newpos.y=MainLoop::screenHeight()-hd;
            dir.y=0;
        }
    }
    pos.x = newpos.x;
    pos.y = newpos.y;
    adjustBox();
}

void Character::draw()
{
    if (!visible)
        return;
    currentImage->draw(pos.x, pos.y);
}

void Character::adjustBox()
{
    collisionbox = Box(pos.x - spritesize.x/2,
              pos.y - spritesize.y/2,
              pos.x + spritesize.x/2,
              pos.y + spritesize.y/2);
}
