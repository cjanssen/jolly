// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __POWERUPS__H__
#define __POWERUPS__H__

#include "powerupmanager.h"

PowerUp *newPowerUp();

// homing
// repulse
// bomb (destroys enemy shots)
// temporary shield
// smaller size
// bigger size
// life
// lasers
// more frequent powerups
// less frequent powerups
// bullets bounce back
// bullets explode x3 in the back
// bigger powerups
// smaller powerups
// freeze
// mothership
// disable multishot

class PU_Doubleshotfreq : public PowerUp
{
public:
    PU_Doubleshotfreq();
    ~PU_Doubleshotfreq();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

class PU_Halfshotfreq : public PowerUp
{
public:
    PU_Halfshotfreq();
    ~PU_Halfshotfreq();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

class PU_Doubleshotspeed : public PowerUp
{
public:
    PU_Doubleshotspeed();
    ~PU_Doubleshotspeed();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

class PU_Halfshotspeed : public PowerUp
{
public:
    PU_Halfshotspeed();
    ~PU_Halfshotspeed();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

//class PU_Biggershot : public PowerUp
//{
//public:
//    PU_Biggershot();
//    ~PU_Biggershot();

//    virtual void activate(Player *who);
//    virtual void disable();
//    virtual const char*description();
//};

//class PU_Smallershot : public PowerUp
//{
//public:
//    PU_Smallershot();
//    ~PU_Smallershot();

//    virtual void activate(Player *who);
//    virtual void disable();
//    virtual const char*description();
//};

class PU_StrongerGravity : public PowerUp
{
public:
    PU_StrongerGravity();
    ~PU_StrongerGravity();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

class PU_WeakerGravity : public PowerUp
{
public:
    PU_WeakerGravity();
    ~PU_WeakerGravity();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

class PU_InvertGravity : public PowerUp
{
public:
    PU_InvertGravity();
    ~PU_InvertGravity();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

class PU_MultiShot : public PowerUp
{
public:
    PU_MultiShot();
    ~PU_MultiShot();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();

};

class PU_BulletWarp : public PowerUp
{
public:
    PU_BulletWarp();
    ~PU_BulletWarp();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();

};

//class PU_BulletBounce : public PowerUp
//{
//public:
//    PU_BulletBounce();
//    ~PU_BulletBounce();

//    virtual void activate(Player *who);
//    virtual void disable();
//    virtual const char*description();

//};

class PU_BackwardShot : public PowerUp
{
public:
    PU_BackwardShot();
    ~PU_BackwardShot();

    virtual void activate(Player *who);
    virtual void disable();
    virtual const char*description();
};

#endif
