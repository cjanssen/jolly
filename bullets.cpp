// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "bullets.h"

#include "player.h"
#include "mainloop.h"
#include "globals.h"

class BulletPrivate {
public:
    BulletPrivate(Bullet *qq):q(qq) {}

    Bullet *q;

    double damage;
    bool dead;
    AnimationInstance image;
    Player *parent;
    Player *target;
    int warpCount;
};

Bullet::Bullet(Vector _pos,
               Vector _dir,
               double _speed,
               double _damage,
               Drawable *bulletpic,
               Player *owner,
               Player *enemy,
               int warps)
    : d(new BulletPrivate(this))
{
    d->damage = _damage;
    d->image = AnimationInstance((Animation *)bulletpic);
    d->parent = owner;
    d->target = enemy;
//    dead = false;
    spritesize = Vector(d->image.width(), d->image.height());
    pos = _pos;
    dir = _dir;
    speed = _speed;
    d->warpCount = warps;
}

Bullet::~Bullet()
{
}

void Bullet::update(double dt)
{
    pos = pos + dir * speed * dt;
    if (pos.x < 0 || pos.x > MainLoop::screenWidth()) {
        if (d->warpCount == 0)
            alive = false;
        else {
            d->warpCount--;
            if (pos.x < 0)
                pos.x += MainLoop::screenWidth();
            if (pos.x > MainLoop::screenWidth())
                pos.x -= MainLoop::screenWidth();
        }
    }

    adjustBox();
    // enemy shot!
    if (d->target->vulnerable && mymath::check_boxinbox(collisionbox, d->target->collisionbox)) {
        alive = false;
        d->target->hit(d->damage);
    }

    // self shot!
    if (d->parent->vulnerable && mymath::check_boxinbox(collisionbox, d->parent->collisionbox)) {
        alive = false;
        d->parent->hit(d->damage);
    }

    if (Globals::instance()->powerUps.check_shot(this->collisionbox)) {
        d->parent->registerPowerup(Globals::instance()->powerUps.last_collided());
        alive = false;
    }


    d->image.update(dt);
}

void Bullet::draw()
{
    d->image.draw(pos.x, pos.y);
}
