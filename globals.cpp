// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "globals.h"

#include "sprites.h"
#include "imageresources.h"
#include "audio.h"
#include "progressbar.h"

#include "SDL/SDL.h"

#include "gamescene.h"

Globals *Globals::globalInstance = 0;

Globals::Globals()
{
}

Globals::~Globals()
{}

Globals *Globals::instance()
{
    if (!globalInstance)
        globalInstance = new Globals;
    return globalInstance;
}

bool Globals::load()
{
    if (!keyboardManager.load())
        return false;

    textManager.setFont("font/visitor2.ttf",36);
    textManager.setColor(Color(255,255,255));


    startKey = Globals::instance()->keyboardManager.newAction(SDLK_SPACE);

    // register key codes
    int KeyP1 = keyboardManager.newAction(SDLK_s);
    int KeyP2 = keyboardManager.newAction(SDLK_k);

    playerLeft.actionUp = keyboardManager.actionBinding(KeyP1);
    playerRight.actionUp = keyboardManager.actionBinding(KeyP2);
    strcpy(playerLeft.ownKey,"S");
    strcpy(playerRight.ownKey,"K");

    playerLeft.currentImage = &(ImageSpace::instance->shipLeft);
    playerLeft.spawn(Vector(256, 150));
    playerLeft.reset();
    playerLeft.otherPlayer = &playerRight;
    playerLeft.shootdir = 1;

    playerRight.currentImage = &(ImageSpace::instance->shipRight);
    playerRight.spawn(Vector(1280 - 256, 150));
    playerRight.reset();
    playerRight.otherPlayer = &playerLeft;
    playerRight.shootdir = -1;
    playerLeft.shootSound = &(ImageSpace::instance->shoot1);
    playerRight.shootSound = &(ImageSpace::instance->shoot2);
    playerLeft.hitSound = &(ImageSpace::instance->hit1);
    playerRight.hitSound = &(ImageSpace::instance->hit2);

//    textManager.setFont("font/visitor2.ttf",24);

    powerUps.load();

#ifdef AUDIOON
    music.setSource("sounds/Song.ogg");
    music.setRepeating(true);
    music.play();
#endif

    GameScene *gameScene = new GameScene();
    gameScene->prepare();
    gameSceneId = sceneManager.append(gameScene);

    sceneManager.switchTo(gameSceneId);

    return true;
}

void Globals::finish()
{
    sceneManager.clear();
}
