// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "entities.h"
#include <stdio.h>
#include <list>

// examples of possibilities:
// timer: has update, but no draw
// bullet: has update and draw
// (static) text: has draw but no update

BaseEntity::BaseEntity()
{
    toRemove = false;
    registerCount = 0;
    EntityManager::registerEntity(this);
}

BaseEntity::~BaseEntity()
{}

void BaseEntity::kill()
{
    toRemove = true;
}

//////////////////////////////////////

class EntityManagerStatic {
public:
    EntityManagerStatic() { looping = false; }
    ~EntityManagerStatic() {}

    std::list<DrawEntity *> drawList;
    std::list<UpdateEntity *> updateList;
    bool looping;
};

EntityManagerStatic entityManagerStatic;

EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{
    clear();
}

void EntityManager::registerEntity(BaseEntity *entity)
{
    if (entity->registerCount != 0) {
        printf("Error: registering duplicated entity!\n");
        return;
    }

    UpdateEntity *asUpdate = dynamic_cast<UpdateEntity *>(entity);
    if (asUpdate) {
        entityManagerStatic.updateList.push_back(asUpdate);
        entity->registerCount++;
    }

    DrawEntity *asDraw = dynamic_cast<DrawEntity *>(entity);
    if (asDraw) {
        entityManagerStatic.drawList.push_back(asDraw);
        entity->registerCount++;
    }

    if (entity->registerCount == 0) {
        printf("Error: registering unreachable entity!\n");
        return;
    }
}

void EntityManager::update(double dt)
{
    for (std::list<UpdateEntity *>::iterator i = entityManagerStatic.updateList.begin();
         i != entityManagerStatic.updateList.end();
         ++i) {
        if ((*i)->active)
            (*i)->update(dt);
        if ((*i)->toRemove) {
            if ((*i)->registerCount == 1)
                delete (*i);
            else
                (*i)->registerCount--;
            entityManagerStatic.updateList.erase(i--);
        }
    }
}

void EntityManager::draw()
{
    for (std::list<DrawEntity *>::iterator i = entityManagerStatic.drawList.begin();
         i != entityManagerStatic.drawList.end();
         ++i) {
        if ((*i)->visible)
            (*i)->draw();
        if ((*i)->toRemove) {
            if ((*i)->registerCount == 1)
                delete (*i);
            else
                (*i)->registerCount--;
            entityManagerStatic.drawList.erase(i--);
        }
    }
}


void EntityManager::clear()
{
    for (std::list<UpdateEntity *>::iterator i = entityManagerStatic.updateList.begin();
         i != entityManagerStatic.updateList.end();
         ++i) {
        try { delete (*i); } catch(...) {}
    }
    for (std::list<DrawEntity *>::iterator i = entityManagerStatic.drawList.begin();
         i != entityManagerStatic.drawList.end();
         ++i) {
        try { delete (*i); } catch(...) {}
    }

    entityManagerStatic.updateList.clear();
    entityManagerStatic.drawList.clear();
}
