// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __IMAGERESOURCES__H__
#define __IMAGERESOURCES__H__

#include "sprites.h"
#include "audio.h"

class ImageSpace {
public:
    ImageSpace();
    ~ImageSpace();
    bool load();

    static ImageSpace *instance;

public:
    Image background;
    Animation bullet;
    Image jumper;
    Animation shipLeft;
    Animation shipRight;
    Image powerup1;
    Image powerup2;
    Image powerup3;
    Image powerup4;
    Animation jet;

    SoundClip shoot1;
    SoundClip shoot2;
    SoundClip hit1;
    SoundClip hit2;
    SoundClip hit3;
};

#endif
