// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SCENES__H__
#define __SCENES__H__

#include "gamestate.h"

class Scene : public State {
public:
    Scene() {}
    virtual ~Scene() {}
    virtual void prepare() = 0;
    virtual void update(double dt) = 0;
    virtual void draw() = 0;
};

class SceneManager : public StateManager {
public:
    SceneManager();
    virtual ~SceneManager();
    virtual int append(State *newState);

    Scene *currentScene() const;
    void update(double dt);
    void draw();
};

#endif // __SCENES__H__
