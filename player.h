// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYER__H__
#define __PLAYER__H__

#include "mymath.h"
#include <list>
#include <vector>
#include "powerupmanager.h"
#include "bullets.h"
#include "audio.h"
#include "progressbar.h"
#include "timers.h"

class Drawable;

class Player : public Character {
public:
    Player();
    ~Player();
    void spawn(const Vector &point);
    void reset();
    void setForTitle();
    void update( double dt );
    void draw();
    void shoot();
    void advanceShootTimer(double dt);
    void adjustBox();
    void hit(double damage);
    void manageHit();

    void registerPowerup(PowerUp *newPowerUp);

    void resetbullets();
    void resetpos();
    void printlives();

    void updateBullets(double dt);
    void updatePowerups(double dt);
    void updatePowerupText(double dt);
    void printPowerupText();
    void printOwnKey();

public:
    bool *actionUp;
    double washit;
    int lives;

    double vspeed;
    double acceleration;
    double gravityAccel;
    double bulletspeed;
    double bulletdamage;
    int bulletsize;
    bool inverseGravity;
    int bulletspershot;
    int bulletWarp;
    int backshot;

    Drawable *currentImage;
    Drawable *bulletpic;
    AnimationInstance jet;

    int shootdir;
    Player *otherPlayer;

    Timer shoot_timer;

    std::list<Bullet> bullets;
    std::list<PowerUp *> powerups;


    SoundClip *shootSound;
    SoundClip *hitSound;

    DrawableProgressBar lifeBar;

    double powerupTextDelay;
    double powerupTextTimer;
    const char* powerupText;
    char* ownKey;

    bool floating;
    bool activeJet;
    double floatThreshold;
    bool vulnerable;

private:
    friend class PowerUp;
};
#endif //__PLAYER__H__
