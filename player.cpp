// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "sprites.h"
#include "player.h"
#include "mainloop.h"
#include "text.h"
#include <stdio.h>
#include <stdlib.h>
#include "powerupmanager.h"
#include "imageresources.h"
#include "globals.h"


Player::Player()
{
    actionUp = 0;
    currentImage = 0;
    bulletpic = 0;
    ownKey = new char[3];
}

Player::~Player()
{
    bullets.clear();
}

// spawn is new player
void Player::spawn(const Vector &point)
{
    bulletpic = &(ImageSpace::instance->bullet);

    alive = true;

    pos = point;
    starting_pos = point;
    spritesize = Vector(currentImage->width(), currentImage->height());
    bulletspeed = 100;
    bulletdamage = 10;
    shoot_timer.setDuration(1);
    shoot_timer.setRepeating(true);
    lives = 5;
    bulletsize = 1;
    bulletspershot = 1;
    bulletWarp = 0;
    backshot = 0;
    lifeBar.setMax(100);
    lifeBar.setFixedDelay(1);

    jet.fromAnimation(&(ImageSpace::instance->jet));

    powerupText = "";
    powerupTextDelay = 1;
    powerupTextTimer = 0;

    floating = false;
    vulnerable = true;
    activeJet = false;
    floatThreshold = 5.0;
    inverseGravity = false;
}

// reset is new life
void Player::reset()
{
    alive = true;

    pos = starting_pos;
    lifeBar.forceCurrent(100);

    acceleration = 50;
    gravityAccel = 25;
    vspeed = 0;

    shoot_timer.reset(shoot_timer.duration() + mymath::unif_rand());
    resetbullets();
}

void Player::setForTitle()
{
    floating = true;
    pos.y = 300;
    vspeed = 0;
    activeJet = false;
}

// reset pos is just position
void Player::resetpos()
{
    alive = true;
    pos = starting_pos;
    vspeed = 0;
    shoot_timer.reset(shoot_timer.duration() + mymath::unif_rand());
    resetbullets();
}

void Player::update( double dt )
{
    Globals *gi = Globals::instance();
    double gravitySign = inverseGravity ? -1 : 1;
    // move up
    if (floating) {
        if (!activeJet && vspeed > floatThreshold) {
            activeJet = true;
        } else {
            if (activeJet && vspeed < -floatThreshold)
                activeJet = false;
        }
        if (activeJet)
            vspeed -= acceleration * dt * gravitySign;
    }
    else {
        if (gi->sceneManager.current() == gi->gameSceneId) {
            if (actionUp && *actionUp)
                vspeed -= acceleration * dt * gravitySign;
        }
    }

    // gravity
        vspeed += gravityAccel * dt * gravitySign;

    pos.y += vspeed;
    adjustBox();
    if (collisionbox.y1 < 0 && vspeed < 0) {
        vspeed = 0;
        pos.y = (pos.y - collisionbox.y1);
    }
    if (collisionbox.y2 > MainLoop::screenHeight() && vspeed > 0)
    {
        vspeed = 0;
        pos.y = MainLoop::screenHeight() - (collisionbox.y2 - pos.y);
    }


    if (gi->sceneManager.current() == gi->gameSceneId) {
        advanceShootTimer(dt);
        updateBullets( dt );
        updatePowerups( dt );


        manageHit();
        lifeBar.setGeometry(pos.x - spritesize.x / 2, pos.y - spritesize.y/4, spritesize.x, 4);
        lifeBar.update(dt);
    }
    currentImage->update(dt);
    jet.update(dt);
}

void Player::adjustBox()
{
    collisionbox = Box(pos.x - 128 + 64,
              pos.y - 128 + 96,
              pos.x - 128 + 192,
              pos.y  - 128 + 192);
}

void Player::updateBullets(double dt)
{
    for (std::list<Bullet>::iterator i = bullets.begin(); i != bullets.end(); ++i) {
        (*i).update(dt);
        if (!(*i).alive)
            bullets.erase(i--);
    }
}

void Player::updatePowerups(double dt)
{
    for (std::list<PowerUp *>::iterator i = powerups.begin(); i != powerups.end(); ++i) {
        (*i)->update(dt);
        if (!(*i)->alive) {
            delete (*i);
            powerups.erase(i--);
        }
    }
    updatePowerupText(dt);
}

void Player::draw()
{
    currentImage->draw(pos.x, pos.y);

    if (actionUp && *actionUp)
        jet.draw(pos.x, pos.y + 64 + 32);

    Globals *gi = Globals::instance();
    if (gi->sceneManager.current() == gi->gameSceneId) {
        for (std::list<Bullet>::iterator i = bullets.begin(); i != bullets.end(); ++i)
            (*i).draw();

        lifeBar.draw();
        printlives();
        printPowerupText();
        printOwnKey();
    }
}

void Player::printlives()
{
    char livetext[128];
    sprintf(livetext,"Lives: %i",lives);
    TextManager::print_once(livetext, pos.x - spritesize.x/2 - 30, 2, Color(255,255,255),"font/visitor2.ttf",36);
}

void Player::advanceShootTimer(double dt)
{
    shoot_timer.update(dt);

    if (shoot_timer.triggered())
        shoot();
}

void Player::shoot()
{
    double y0 = pos.y - (spritesize.y / 16) * (bulletspershot-1);
    int oldwarp = bulletWarp;
    if (backshot) {
        shootdir = -shootdir;
        bulletWarp = (bulletWarp == 0? 1 : bulletWarp);
    }
    for (int ii = 0; ii < bulletspershot; ii++)
        bullets.push_back(
                Bullet(
                    Vector(pos.x + spritesize.x * 0.5 * shootdir, y0 + spritesize.y/8*ii),
                    Vector(shootdir,0),
                    bulletspeed,
                    bulletdamage,
                    bulletpic,
                    this,
                    otherPlayer,
                    bulletWarp));

    // this is hacked!
    if (backshot) {
        shootdir = -shootdir;
        bulletWarp = oldwarp;
    }

    currentImage->reset();
#ifdef AUDIOON
    shootSound->play();
#endif
}

void Player::hit(double damage)
{
    washit += damage;
}

void Player::manageHit()
{
    if (washit) {
#ifdef AUDIOON
        hitSound->play();
#endif
        lifeBar.setCurrent(lifeBar.current() - washit);
        washit = 0;

        if (lifeBar.current() == 0) {
            resetbullets();
            otherPlayer->resetbullets();
            lifeBar.forceCurrent(100);

            lives--;
            if (lives == 0) {
                spawn(starting_pos);
                otherPlayer->reset();
            }
        }
    }
}

void Player::registerPowerup(PowerUp *newPowerUp)
{
    powerups.push_back(newPowerUp);
    newPowerUp->activate(this);
    powerupTextTimer = powerupTextDelay;
    powerupText = newPowerUp->description();
}

void Player::resetbullets()
{
    bullets.clear();
}

void Player::updatePowerupText(double dt)
{
    if (powerupTextTimer <= 0)
        return;
    powerupTextTimer -= dt;
}

void Player::printPowerupText()
{
    if (powerupTextTimer > 0)
        TextManager::print_once(powerupText,
                            pos.x - spritesize.x/2 - 30,
                            -30,
                            Color(255,255,255),
                            "font/visitor2.ttf",
                            36);
}

void Player::printOwnKey()
{
    TextManager::print_once(ownKey, pos.x - 18, pos.y + 18,
                            Color(255,255,255),"font/visitor2.ttf", 36);
}

