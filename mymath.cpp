// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "mymath.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>

Matrix mymath::get_rotation_matrix( double angle )
{
    register double c = cos(angle);
    register double s = sin(angle);
    return Matrix(c, -s, s, c);
}

Vector mymath::rotate(const Vector &vector, double angle)
{
    Matrix m = mymath::get_rotation_matrix(angle);
    return vector * m;
}
Vector mymath::disturb_vector(const Vector &vector, double deviation)
{
    Vector direction = vector + mymath::randn2() * deviation;
    return mymath::get_dir_vector( 0, 0, direction.x, direction.y );
}

double mymath::sign( double a )
{
    if (a>=0) { return 1; }
    return -1;
}

double mymath::round( double r )
{
    return floor(r+0.5);
}

double mymath::min(double a, double b) {
    return a<b ? a : b;
}

double mymath::max(double a, double b) {
    return a>b ? a : b;
}

Vector mymath::normalize_vector( const Vector &vec )
{
    double mag = vec.x*vec.x + vec.y*vec.y;
    if (mag != 1.0 && mag != 0.0)
        return vec * (1/sqrt(mag));
    else
        return vec;
}

double mymath::get_angle( const Vector &v1, const Vector &v2 )
{
    Vector nv1 = mymath::normalize_vector( v1 );
    Vector nv2 = mymath::normalize_vector( v2 );
    double cosangle = acos( nv1.x*nv2.x + nv1.y*nv2.y );
    double sinangle = asin( nv1.x*nv2.y - nv1.y*nv2.x );

    return cosangle*mymath::sign(sinangle);
}

void mymath::init_random()
{
    srand((int)time(0));
}

double mymath::unif_rand()
{
    return random()/(double)RAND_MAX;
}

double mymath::randn()
{
    double x1,x2;
    double w = 1;
    while (w >= 1 && w>0) {
        x1 = 2 * mymath::unif_rand() - 1;
        x2 = 2 * mymath::unif_rand() - 1;
        w = x1*x1 + x2*x2;
    }

    w = sqrt( (-2 * log( w ) / w ) );
    return x1 * w;
}
Vector mymath::randn2()
{
    double x1,x2;
    double w = 1;
    while (w >= 1 && w>0) {
        x1 = 2 * mymath::unif_rand() - 1;
        x2 = 2 * mymath::unif_rand() - 1;
        w = x1*x1 + x2*x2;
    }

    w = sqrt( (-2 * log( w ) / w ) );
    return Vector(x1 * w, x2 * w);
}

void mymath::permutation( int * result, int len )
{
    bool *used = new bool[len];
    int i;
    for (i=0; i<len; i++) {
        used[i] = false;
    }

    int j=0;
    for (i=0; i<len; i++) {
        int newnum = random()%(len-i);
        while (newnum>=0) {
            j = (j+1) % len;
            if (!used[j]) {
                newnum--;
            }
        }
        used[j] = true;
        result[i]=j+1;
    }
    delete used;
}

double mymath::get_distance(const Vector &point1,const Vector &point2)
{
    double dx = point2.x - point1.x;
    double dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy);
}

double mymath::get_distanceSq(const Vector &point1, const Vector &point2)
{
    double dx = point2.x - point1.x;
    double dy = point2.y - point1.y;
    return (dx*dx + dy*dy);
}


bool mymath::check_intersection(const Box &segment,const Box &box)
{
    if (segment.x1>= box.x1 && segment.x1<=box.x2 && segment.y1>=box.y1 && segment.y1<=box.y2) {
        return true;
    }

    if (segment.x2>= box.x1 && segment.x2<=box.x2 && segment.y2>=box.y1 && segment.y2<=box.y2) {
        return true;
    }

    if (! mymath::check_boxes(
                Box(mymath::min(segment.x1,segment.x2), mymath::min(segment.y1,segment.y2),
                    mymath::max(segment.x1,segment.x2), mymath::max(segment.y1,segment.y2) ),
                box
                ) ){
        return false;
    }


    return mymath::check_intersection_internal( segment, box );

}

bool mymath::check_intersection_internal( const Box &segment, const Box &box )
{
    double dX = segment.x2 - segment.x1;
    double dY = segment.y2 - segment.y1;

    if (fabs(dX) > 0.001) {
        double tL = (box.x1-segment.x1)/dX;
        if (tL >= 0  && tL <= 1)
        {
            double yL = segment.y1 + tL*dY;
            if (yL <= box.y2 && yL >= box.y1) {
                return true;
            }
        }


        double tR = (box.x2-segment.x1)/dX;
        if (tR >= 0  && tR <= 1)
        {
            double yR = segment.y1 + tR*dY;
            if (yR <= box.y2 && yR >= box.y1) {
                return true;
            }
        }

    }

    if (fabs(dY) > 0.001) {
        {
            double tU = (box.y1-segment.y1)/dY;
            if (tU >= 0  && tU <= 1)
            {
                double xU = segment.x1 + tU*dX;
                if (xU >= box.x1 && xU <= box.x2) {
                    return true;
                }
            }


            double tD = (box.y2-segment.y1)/dY;
            if (tD >= 0  && tD <= 1)
            {
                double xD = segment.x1 + tD*dX;
                if (xD >= box.x1 && xD <= box.x2) {
                    return true;
                }
            }
        }

        return false;
    }

    return mymath::check_pointinbox(Vector(segment.x1,segment.y1), box);
}


bool mymath::check_boxes(const Box &box1,const Box &box2)
{
    if (box1.x2<box2.x1 ||
            box1.x1>box2.x2 ||
            box1.y1>box2.y2 ||
            box1.y2<box2.y1) {
        return false;
    }
    return true;
}

bool mymath::check_boxinbox(const Box &small_box, const Box &big_box)
{
    if (small_box.x1 < big_box.x1 ||
            small_box.y1 < big_box.y1 ||
            small_box.x2 > big_box.x2 ||
            small_box.y2 > big_box.y2) {
        return false;
    }
    return true;
}

bool mymath::check_pointinbox(const Vector &point, const Box &box)
{
    if (point.x >= box.x1 && point.x <= box.x2 &&
            point.y >= box.y1 && point.y <= box.y2)
        return true;
    else
        return false;
}

Vector mymath::get_dir_vector(const Vector &from, const Vector &to)
{
    return get_dir_vector(from.x, from.y, to.x, to.y);

}

Vector mymath::get_dir_vector(double origx, double origy, double destx, double desty)
{
    Vector destv(destx - origx, desty - origy);
    double modul = sqrt( destv.x*destv.x + destv.y*destv.y );
    if (modul < 0.001) {
        modul = 0.001;
    }
    return Vector( destv.x/modul, destv.y/modul );
}


bool mymath::check_pointinbuilding(const Vector &point,const Building &building )
{
    for (int i=0; i<building.count; i++) {
        if (mymath::check_pointinbox(point, building.collision[i])) {
            return true;
        }
    }
    return false;
}

bool mymath::check_segmentinbuilding(const Box &segment,const Building &building )
{
    for (int i=0; i<building.count; i++) {
        if (mymath::check_intersection(segment, building.collision[i])) {
            return true;
        }
    }
    return false;
}

Triplet mymath::distanceSq_to_box( const Vector &point, const Box &box )
{
    if (check_pointinbox(point, box))
        return Triplet(0, point.x, point.y);
    if (point.x >= box.x1 && point.x<= box.x2) {
        if (point.y < box.y1) {

            double diff = box.y1 - point.y;
            return Triplet(diff*diff, point.x, box.y1);
        }
        else {
            double diff = box.y2 - point.y;
            return Triplet(diff*diff, point.x, box.y2);
        }
    }

    if (point.y >= box.y1 && point.y<= box.y2) {
        if (point.x < box.x1) {
            double diff = box.x1 - point.x;
            return Triplet(diff*diff, box.x1, point.y);
        }    else {

            double diff = box.x2 - point.x;
            return Triplet(diff*diff, box.x2, point.y);
        }
    }

    double p1;
    if (point.x < box.x1) {
        p1 = box.x1;
    } else {
        p1 = box.x2;
    }

    double p2;
    if (point.y < box.y1) {
        p2 = box.y1;
    } else {
        p2 = box.y2;
    }

    double d1 = p1 - point.x;
    double d2 = p2 - point.y;
    return Triplet(d1*d1+d2*d2, p1, p2);
}

bool mymath::check_boxinbuilding( const Box &box1, const Building &building )
{
    for (int i=0; i<building.count; i++) {
        if (mymath::check_boxes(box1, building.collision[i])) {
            return true;
        }
    }
    return false;
}
