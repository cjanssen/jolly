// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "gamescene.h"
#include "globals.h"
#include "mainloop.h"
#include "imageresources.h"

#include "SDL/SDL.h"

GameScene::GameScene()
{
}

GameScene::~GameScene()
{
}

void GameScene::prepare()
{
}

void GameScene::enable()
{
}

void GameScene::disable()
{
}

void GameScene::update(double dt)
{
    Globals *gi = Globals::instance();

    gi->playerLeft.update(dt);
    gi->playerRight.update(dt);
    gi->powerUps.update(dt);

}

void GameScene::draw()
{
    SDL_Surface *sc = MainLoop::screenHandle();
    Globals *gi = Globals::instance();

    //SDL_FillRect(sc, NULL, SDL_MapRGB( sc->format, 0, 0, 0));
    ImageSpace::instance->background.draw(0,0, sc, false);

    gi->playerLeft.draw();
    gi->playerRight.draw();
    gi->powerUps.draw();
}
