// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MOVEMENT__H__
#define __MOVEMENT__H__

#include "mymath.h"
#include "sprites.h"


class List;
class Character {
public:
    Character();
    virtual ~Character();
    virtual void update(double dt);
    virtual void draw();
    virtual void adjustBox();
    // TODO: put some pure virtual function

    bool alive;
    bool visible;

    Vector starting_pos;
    Vector pos;
    Vector dir;
    double speed;
    Vector spritesize;
    Box collisionbox;

    Drawable *currentImage;

    bool canexitScreen;
};


#endif //__MOVEMENT__H__
