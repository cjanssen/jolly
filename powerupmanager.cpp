// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "powerupmanager.h"
#include <vector>
#include "mainloop.h"
#include <stdio.h>
#include "player.h"
#include "powerups.h"
#include "imageresources.h"
#include "timers.h"
#include "globals.h"

PowerUp::PowerUp()
{
    currentImage = 0;
    pos.x = MainLoop::screenWidth()/2;
    pos.y = 0;
    dir.x = 0;
    dir.y = 1;
    speed = 100;
    canexitScreen = true;
    spritesize.x = 0;
    spritesize.y = 0;
    activeDelay = 10;
    activeTimer = 0;
    active = false;
    owner = 0;
}

PowerUp::~PowerUp()
{
}

void PowerUp::resetPos()
{
    pos.x = MainLoop::screenWidth()/2;
    pos.y = -(currentImage->height()/2);
    spritesize.x = currentImage->width();
    spritesize.y = currentImage->height();
}

void PowerUp::update( double dt )
{
    Character::update(dt);
    if (pos.y - currentImage->height()/2 > MainLoop::screenHeight())
        alive = false;

    // extra
    if (active) {
        extraUpdate(dt);
        activeTimer -= dt;
        if (activeTimer <= 0) {
            disable();
        }
    }
}

void PowerUp::draw()
{
    Character::draw();
}

void PowerUp::activate(Player *who)
{
    active = true;
    visible = false;
    activeTimer = activeDelay;
    owner = who;
    dir = Vector(0,0);
#ifdef AUDIOON
    ImageSpace::instance->hit3.play();
#endif
}

void PowerUp::disable()
{
    active = false;
    alive = false;
}

void PowerUp::extraUpdate(double dt)
{
}


class PowerUpManagerPrivate
{
public:
    PowerUpManagerPrivate(PowerUpManager *qq):q(qq) {}

    PowerUpManager *q;
    std::vector<PowerUp *> powerups;
    Timer spawnTimer;
    int colBegin;
    int colEnd;

    PowerUp *touchedPowerUp;
};

typedef std::vector<PowerUp *>::iterator PowerUpIterator;

PowerUpManager::PowerUpManager() : d(new PowerUpManagerPrivate(this))
{
    d->spawnTimer.setDuration(2);
    d->spawnTimer.setRepeating(true);
}

PowerUpManager::~PowerUpManager()
{
}

void PowerUpManager::load()
{
    //d->powerupImage.load("images/pow.png");
    d->colBegin = MainLoop::screenWidth()/2; // - d->powerupImage.width()/2;
    d->colEnd = d->colBegin; // + d->powerupImage.width()/2;
}

void PowerUpManager::adjustColumnWidth()
{
    int increment = 0;
    for (PowerUpIterator i = d->powerups.begin();
         i != d->powerups.end();
         ++i) {
        int ww = (*i)->spritesize.x;
        if (ww > increment)
            increment = ww;
    }
    d->colBegin = MainLoop::screenWidth() / 2 - increment;
    d->colEnd = MainLoop::screenWidth() / 2 + increment;
}

void PowerUpManager::update( double dt )
{
    if (update_spawnTimer( dt ))
        spawn();

    for (PowerUpIterator i = d->powerups.begin();
         i != d->powerups.end();
         ++i) {
        (*i)->update(dt);
        if (!(*i)->alive) {
            delete (*i);
            d->powerups.erase(i--);
            adjustColumnWidth();
        }
    }
}

bool PowerUpManager::update_spawnTimer( double dt )
{
    d->spawnTimer.update(dt);
    return d->spawnTimer.triggered();
}

void PowerUpManager::draw()
{

    for (PowerUpIterator i = d->powerups.begin();
         i != d->powerups.end();
         ++i) {
        (*i)->draw();
    }
}

void PowerUpManager::spawn()
{
    d->powerups.push_back(newPowerUp());
    adjustColumnWidth();
}

bool PowerUpManager::check_shot(const Box &b)
{
    // by now assume all powerup boxes are the same
    if (b.x2 >= d->colBegin && b.x1 <= d->colEnd) {
        for (PowerUpIterator i = d->powerups.begin();
             i != d->powerups.end();
             ++i) {
            if (mymath::check_boxes(b, (*i)->collisionbox)) {
                d->touchedPowerUp = (*i);
                d->powerups.erase(i--);
                return true;
            }
        }
    }

    return false;
}

PowerUp *PowerUpManager::last_collided()
{
    PowerUp *retVal = d->touchedPowerUp;
    d->touchedPowerUp = 0;
    return retVal;
}

