// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "audio.h"

// Using SDL because I've got weird problems with QtMultimedia
#include <SDL/SDL.h>
#ifdef __APPLE__
    #include <SDL_mixer/SDL_mixer.h>
#else
    #include <SDL/SDL_mixer.h>
#endif

#include <vector>
#include <algorithm>

void musicFinished();

void initMusicControl();

// Mixer Instance
bool StartMixer() {
    int audio_rate = 44100;
    Uint16 audio_format = AUDIO_S16SYS;
    int audio_channels = 2;
    int audio_buffers = 4096;

    if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
        printf("Unable to initialize audio: %s\n", Mix_GetError());
        return false;
    }

    initMusicControl();

    Mix_HookMusicFinished(musicFinished);
    return true;
}

void CloseMixer() {
    Mix_CloseAudio();
}

MixerInstance::MixerInstance()
{
    valid = StartMixer();
}

MixerInstance::~MixerInstance()
{
    if (valid) {
        CloseMixer();
    }
}

bool MixerInstance::isValid() const
{
    return valid;
}

MixerInstance mixerInstance;

///////////////////////////////////////////////////////////////////////////
// Sound Clip Object
class SoundClipPrivate {
public:
    const char *fileName;
    Mix_Chunk *sound;
    int channel;
    int volume; // 0 to 128
};

SoundClip::SoundClip()
{
    d = new SoundClipPrivate;
    d->sound = 0;
    d->channel = -1;
    d->volume = 128;
}

SoundClip::~SoundClip()
{
    stop();
    if (d->sound) {
        Mix_FreeChunk(d->sound);
    }
    delete d;
}

const char *SoundClip::source() const
{
    return d->fileName;
}

void SoundClip::setSource(const char *newSource)
{
    if (playing())
        stop();

    if (source() != newSource) {
        if (d->sound) {
            Mix_FreeChunk(d->sound);
        }
        d->sound = Mix_LoadWAV(newSource);
        if (!d->sound) {
                printf("Unable to load WAV file %s: %s\n", newSource, Mix_GetError());
        } else {
            d->fileName = newSource;
        }
    }
}

bool SoundClip::playing() const
{
    return d->channel != -1 && Mix_Playing(d->channel);
}

void SoundClip::play()
{
    if (!d->sound)
        return;

    d->channel = Mix_PlayChannel(-1, d->sound, 0);
    if(d->channel == -1) {
        // silent error
        //qDebug() << "Unable to play WAV file" << d->fileName << ":" << Mix_GetError();
    } else {
        Mix_Volume(d->channel, d->volume);
    }
}

void SoundClip::stop()
{
    // actually we should store a list of channels and track when they're done...
    if (d->channel != -1) {
        Mix_HaltChannel(d->channel);
        d->channel = -1;
    }
}

int SoundClip::volume() const
{
    return d->volume;
}

void SoundClip::setVolume(int newVolume)
{
    if (d->volume != newVolume) {
        d->volume = newVolume;
        if (d->volume > 128)
            d->volume = 128;
        if (d->volume < 0)
            d->volume = 0;
        if (playing())
            Mix_Volume(d->channel, d->volume);
    }
}

///////////////////////////////////////////////////////////////////////////
// Music Clip Object
class MusicClipPrivate {
public:
    const char *fileName;
    Mix_Music *music;
    bool isPlaying;
    int loops;
    int fadeInTime;
    bool willRepeat;
    int volume;
};

struct MusicControl {
    std::vector<MusicClip *> allMusicInstances;
    std::vector<MusicClip *> musicQueue;
    int musicCount;
    bool repeatCurrent;
    MusicClip * currentlyPlaying;
} musicControl;

void initMusicControl()
{
    musicControl.musicCount = 0;
    musicControl.repeatCurrent = false;
    musicControl.currentlyPlaying = 0;
}

void musicFinished() {
    for(std::vector<MusicClip *>::iterator i = musicControl.allMusicInstances.begin();
        i != musicControl.allMusicInstances.end(); ++i)
        (*i)->notifyFinish();

    if (musicControl.repeatCurrent && musicControl.currentlyPlaying)
        musicControl.currentlyPlaying->play();

    if (musicControl.musicCount == 0 && musicControl.musicQueue.size() != 0) {
        std::vector<MusicClip *>::iterator nextSong = musicControl.musicQueue.begin();
        musicControl.musicQueue.erase(nextSong);
        (*nextSong)->play();
    }
}


MusicClip::MusicClip()
{
    d = new MusicClipPrivate;
    d->music = 0;
    d->isPlaying = false;
    d->loops = 0;
    d->fadeInTime = 0;
    d->willRepeat = false;
    d->volume = 128;
    musicControl.allMusicInstances.push_back(this);
}

MusicClip::~MusicClip()
{
    stop();
    if (d->music) {
        Mix_FreeMusic(d->music);
        d->music = 0;
    }

    std::vector<MusicClip *>::iterator musicPos = std::find(musicControl.allMusicInstances.begin(),
                                                        musicControl.allMusicInstances.end(),
                                                        this);
    if (musicPos != musicControl.allMusicInstances.end())
        musicControl.allMusicInstances.erase(musicPos);
    musicPos = std::find(musicControl.musicQueue.begin(),
                         musicControl.musicQueue.end(),
                         this);
    if (musicPos != musicControl.musicQueue.end())
        musicControl.musicQueue.erase(musicPos);
    delete d;
}

const char *MusicClip::source() const
{
    return d->fileName;
}

void MusicClip::setSource(const char *newSource)
{
    if (playing())
        stop();

    if (source() != newSource) {
        if (d->music) {
            Mix_FreeMusic(d->music);
        }
        d->music = Mix_LoadMUS(newSource);
            if(!d->music) {
                printf("Unable to load Music file: %s\n", Mix_GetError());
        } else {
            d->fileName = newSource;
        }
    }
}

int MusicClip::loops() const
{
    return d->loops;
}

void MusicClip::setLoops(int loops)
{
    if (d->loops != loops) {
        d->loops = loops;
    }
}

int MusicClip::fadeInTime() const
{
    return d->fadeInTime;
}

void MusicClip::setFadeInTime(int ms)
{
    if (d->fadeInTime != ms) {
        d->fadeInTime = ms;
    }
}

void MusicClip::notifyFinish()
{
    if (d->isPlaying) {
        d->isPlaying = false;
        musicControl.musicCount--;
    }
}
bool MusicClip::playing() const
{
    return d->isPlaying;
}

void MusicClip::play()
{
    if (!d->music)
        return;
    if (Mix_FadeInMusic(d->music, d->loops, d->fadeInTime) == -1) {
        printf("Unable to play Music file: %s\n", Mix_GetError());
        return;
    }
    Mix_VolumeMusic(d->volume);
    d->isPlaying = true;
    musicControl.musicCount++;
    musicControl.currentlyPlaying = this;
    musicControl.repeatCurrent = d->willRepeat;
}

void MusicClip::stop()
{
    if (d->isPlaying) {
        Mix_HaltMusic();
        musicFinished();
    }
}

void MusicClip::enqueue()
{
    if (musicControl.musicCount == 0)
        play();
    else
        musicControl.musicQueue.push_back(this);
}

void MusicClip::unqueue()
{
    std::vector<MusicClip *>::iterator musicPos = std::find(musicControl.musicQueue.begin(),
                                                            musicControl.musicQueue.end(),
                                                            this);
    if (musicPos != musicControl.musicQueue.end())
        musicControl.musicQueue.erase(musicPos);
}

void MusicClip::fadeOut(int ms)
{
    if (d->isPlaying)
        Mix_FadeOutMusic(ms);
}

bool MusicClip::repeating() const
{
    return d->willRepeat;
}

void MusicClip::setRepeating(bool repeating)
{
    if (d->willRepeat != repeating) {
        d->willRepeat = repeating;
        if (d->isPlaying)
            musicControl.repeatCurrent = repeating;
    }
}

int MusicClip::volume() const
{
    return d->volume;
}

void MusicClip::setVolume(int newVolume)
{
    if (d->volume != newVolume) {
        d->volume = newVolume;
        if (d->volume > 128)
            d->volume = 128;
        if (d->volume < 0)
            d->volume = 0;
        if (playing())
            Mix_VolumeMusic(d->volume);
    }
}
