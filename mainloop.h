// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAINLOOP__H__
#define __MAINLOOP__H__

#define __SHOW_FPS__

struct SDL_Surface;

class MainLoop {
public:
    MainLoop();
    ~MainLoop();

    bool init();
    void splashScreen();

    virtual bool load();
    void start();
    virtual void close();

    static SDL_Surface *screenHandle();
    static int screenWidth();
    static int screenHeight();
    static void requestExit();
    static void setTimestep(int newTimestep);
    static void toggleFPS();

    virtual void draw();
    virtual void update(double dt);
    virtual void onKeyPressed(int key) = 0;
    virtual void onKeyReleased(int key) = 0;
    virtual void onMouseMove(int x, int y) = 0;
    virtual void onMousePressed(int x, int y, int button) = 0;
    virtual void onMouseReleased(int x, int y, int button) = 0;

    static int mouseX();
    static int mouseY();

private:
    class MainLoopPrivate;
    MainLoopPrivate *d;
};

#endif //__MAINLOOP__H__
