// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "mainloop.h"


#include "SDL/SDL.h"
//#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"

// needed for error messages
#include <stdio.h>
#include "text.h"

class MainLoop::MainLoopPrivate {
public:
    MainLoopPrivate(MainLoop *qq):q(qq){}
    MainLoop *q;

    static SDL_Surface* screen;
    static bool exit;
    static bool showFPS;

    static long int updateStep; // milliseconds simulation update (ms)
    long int lastTime; // last iteration time (ms)
    long int dt; // elapsed time (ms)
};

SDL_Surface* MainLoop::MainLoopPrivate::screen = 0;
bool MainLoop::MainLoopPrivate::exit = false;
bool MainLoop::MainLoopPrivate::showFPS = false;
long int MainLoop::MainLoopPrivate::updateStep = 10;

MainLoop::MainLoop():d(new MainLoopPrivate(this))
{
}

MainLoop::~MainLoop()
{
    delete d;
}

bool MainLoop::init()
{
    //Start SDL
    if (SDL_Init( SDL_INIT_EVERYTHING ) != 0) {
        printf("Unable to initialize SDL: %s\n", SDL_GetError());
        return false;
    }

    //Set up screen
    d->screen = SDL_SetVideoMode( 1280, 720, 32, SDL_SWSURFACE | SDL_FULLSCREEN );
    // if fullscreen doesn't work, try windowed
    if (!d->screen)
        d->screen = SDL_SetVideoMode( 1280, 720, 32, SDL_SWSURFACE);
    if (!d->screen) {
        printf("Unable to set video mode: %s\n", SDL_GetError());
        return false;
    }

    if (TTF_Init() != 0) {
        printf("Failed to initialize TTF module: %s\n", TTF_GetError());
        return false;
    }

    d->exit = false;
    d->updateStep = 30;
    d->lastTime = SDL_GetTicks();
    d->dt = 0;

    return true;
}

bool MainLoop::load()
{
    return true;
}

void MainLoop::start()
{
    SDL_Event event;

    while (!d->exit) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_KEYDOWN: {
                onKeyPressed(event.key.keysym.sym);
                break;
            }
            case SDL_KEYUP: {
                onKeyReleased(event.key.keysym.sym);
                break;
            }
            case SDL_MOUSEBUTTONDOWN: {
                onMousePressed(event.button.x, event.button.y, event.button.button);
                break;
            }
            case SDL_MOUSEBUTTONUP: {
                onMouseReleased(event.button.x, event.button.y, event.button.button);
                break;
            }
            case SDL_MOUSEMOTION: {
                onMouseMove(event.button.x, event.button.y);
                break;
            }
            case SDL_QUIT: {
                requestExit();
                break;
            }
            }
        }

        // time control
        long int newTime = SDL_GetTicks();
        d->dt += newTime - d->lastTime;
        d->lastTime = newTime;

        while(d->dt > d->updateStep) {
            update( d->updateStep/1000.0 );
            d->dt -= d->updateStep;
        }

        draw();
    }
}

void MainLoop::requestExit()
{
    MainLoopPrivate::exit = true;
}

void MainLoop::toggleFPS()
{
    MainLoopPrivate::showFPS = !MainLoopPrivate::showFPS;
}

void MainLoop::close()
{
    // Release the screen
    SDL_FreeSurface( d->screen );

    // Close font system
    TTF_Quit();

    // Close SDL subsystems
    SDL_Quit();
}


void MainLoop::setTimestep( int newTimestep )
{
    MainLoopPrivate::updateStep = newTimestep;
}

SDL_Surface *MainLoop::screenHandle()
{
    return MainLoopPrivate::screen;
}

int MainLoop::screenWidth()
{
    return MainLoopPrivate::screen->w;
}

int MainLoop::screenHeight()
{
    return MainLoopPrivate::screen->h;
}

void MainLoop::draw()
{
#ifdef __SHOW_FPS__
    if (MainLoopPrivate::showFPS) {
        static long int lasttime = SDL_GetTicks();
        long int delay = SDL_GetTicks() - lasttime;
        lasttime += delay;
        static double currentDelay = -1;
        // Doesn't filter for below 10FPS to avoid false negatives
        if (currentDelay < 0 || currentDelay > 100)
            currentDelay = delay;
        else
            currentDelay = 0.99*currentDelay + 0.01*delay;

        if (currentDelay > 0) {
            char fpsString[128];
            sprintf(fpsString,"%.2f FPS",1000.0/currentDelay);
            SDL_WM_SetCaption(fpsString,0);
        }
    }
#endif

    //Update Screen
    SDL_Flip( d->screen );
}

void MainLoop::update(double dt)
{
}

void MainLoop::splashScreen()
{
    TextManager::print_once("Loading...", -32, -32, Color(255,255,255), "font/visitor2.ttf", 48);
    SDL_Flip(screenHandle());
}

// Todo: this should be tracked by the events
int MainLoop::mouseX()
{
    int x,y;
    SDL_GetMouseState(&x, &y);
    return x;
}

int MainLoop::mouseY()
{
    int x,y;
    SDL_GetMouseState(&x, &y);
    return y;
}
