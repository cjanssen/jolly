// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ENTITIES__H__
#define __ENTITIES__H__

class BaseEntity {
public:
    BaseEntity();
    virtual ~BaseEntity();
    virtual void kill();

protected:
    friend class EntityManager;
    bool toRemove;
    int registerCount;
};

class DrawEntity : public BaseEntity {
public:
    DrawEntity() {}
    virtual ~DrawEntity() {}
    virtual void draw() = 0;

    bool visible; // turn off to skip drawing
    double absoluteZ; // painting order (from minor to major)
};

class UpdateEntity : public BaseEntity {
public:
    UpdateEntity() {}
    virtual ~UpdateEntity() {}
    virtual void update( double dt ) = 0;

    bool active; // turn off to stop updates
};


class EntityManager {
public:
    EntityManager();
    ~EntityManager();

    static void registerEntity(BaseEntity *entity);
    static void update(double dt);
    static void draw();
    static void clear();
};
#endif
