// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "progressbar.h"

#include "SDL/SDL.h"
#include "mainloop.h"

class ProgressBarPrivate {
public:
    int x;
    int y;
    int w;
    int h;
    double min;
    double max;
    double currentVal;
    double interpVal;
    double initVal;
    double speed;
    double delay;
    double timer;
    ProgressBar::Direction dir;
};

ProgressBar::ProgressBar() : d(new ProgressBarPrivate())
{
    d->x = 0;
    d->y = 0;
    d->w = 0;
    d->h = 0;
    d->max = 1;
    d->min = 0;
    d->dir = LeftToRight;
    forceCurrent(0);
    setFixedSpeed(1);
}

ProgressBar::~ProgressBar()
{
}

void ProgressBar::update(double dt)
{
    if (d->currentVal == d->interpVal)
        return;
    if (d->delay == -1) { // fixed speed
        if (d->currentVal > d->interpVal) { // grow
            d->interpVal += d->speed * dt;
            if (d->interpVal > d->currentVal)
                d->interpVal = d->currentVal;
        } else { // shrink
            d->interpVal -= d->speed * dt;
            if (d->interpVal < d->currentVal)
                d->interpVal = d->currentVal;
        }
    } else if (d->delay > 0) { // fixed delay
        d->timer -= dt;
        if (d->timer < 0)
            d->timer = 0;
        double distance = d->currentVal - d->initVal;
        double fraction = (d->delay - d->timer) / d->delay;
        d->interpVal = d->initVal + fraction * distance;
    } else { // instantaneous
        d->interpVal = d->currentVal;
    }
}

void ProgressBar::setMax(double max)
{
    d->max = max;
}

void ProgressBar::setMin(double min)
{
    d->min = min;
}

void ProgressBar::setCurrent(double current)
{
    d->currentVal = current;
    if (d->currentVal > d->max)
        d->currentVal = d->max;
    if (d->currentVal < d->min)
        d->currentVal = d->min;
    d->timer = d->delay;
    d->initVal = d->interpVal;
}

double ProgressBar::current() const
{
    return d->currentVal;
}

void ProgressBar::forceCurrent(double current)
{
    setCurrent(current);
    d->interpVal = current;
    d->initVal = current;
}

void ProgressBar::setFixedSpeed(double speed)
{
    // units per second
    d->speed = speed;
    d->delay = -1;
}

void ProgressBar::setFixedDelay(double delay)
{
    d->delay = delay;
    d->timer = d->delay;
    d->speed = -1;
}

void ProgressBar::setGeometry(int x, int y, int w, int h)
{
    d->x = x;
    d->y = y;
    d->w = w;
    d->h = h;
}

void ProgressBar::setDirection(ProgressBar::Direction dir)
{
    d->dir = dir;
}

/////////////////////////////////////////////////////////////////
DrawableProgressBar::DrawableProgressBar()
{
    borderWidth = 2;
}

DrawableProgressBar::~DrawableProgressBar()
{}

void DrawableProgressBar::draw()
{
    SDL_Rect rect = {d->x, d->y, d->w, d->h};
    rect.w = d->w * (d->interpVal - d->min) / (d->max - d->min);

    SDL_Surface *sc = MainLoop::screenHandle();
    SDL_FillRect(sc, &rect, SDL_MapRGB(sc->format, 128, 255, 0));

    if (borderWidth) {
        SDL_Rect borderUp = {d->x - borderWidth, d->y - borderWidth, d->w + borderWidth * 2, borderWidth};

        SDL_FillRect(sc, &borderUp, SDL_MapRGB(sc->format, 0, 0, 0));
        SDL_Rect borderDown = {d->x - borderWidth, d->y + d->h, d->w + borderWidth * 2, borderWidth};
        SDL_FillRect(sc, &borderDown, SDL_MapRGB(sc->format, 0, 0, 0));

        SDL_Rect borderLeft = {d->x - borderWidth, d->y, borderWidth, d->h};
        SDL_FillRect(sc, &borderLeft, SDL_MapRGB(sc->format, 0, 0, 0));
        SDL_Rect borderRight = {d->x + d->w, d->y, borderWidth, d->h};
        SDL_FillRect(sc, &borderRight, SDL_MapRGB(sc->format, 0, 0, 0));
    }
}
