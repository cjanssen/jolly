// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "imageresources.h"

ImageSpace img;
ImageSpace *ImageSpace::instance = &img;

ImageSpace::ImageSpace()
{
}

ImageSpace::~ImageSpace()
{
}

bool ImageSpace::load()
{
    background.load("images/BG.png");
    bullet.init("images/BulletA.png",32, 32, 0.06, true);
    jumper.load("images/dude.png");

    powerup1.load("images/PowerUpA.png");
    powerup2.load("images/PowerUpB.png");
    powerup3.load("images/PowerUpC.png");
    powerup4.load("images/PowerUpD.png");

    shipLeft.init("images/ShipA.png",256,256, 0.25, false);
    shipLeft.forceFrame(1);
    shipRight.init("images/ShipB.png",256,256, 0.25, false);
    shipRight.forceFrame(1);

    jet.init("images/Jets.png", 64, 64, 0.06, true);

    shoot1.setSource("sounds/Booms3.ogg");
    shoot2.setSource("sounds/Booms4.ogg");
    hit1.setSource("sounds/Booms1.ogg");
    hit2.setSource("sounds/Booms2.ogg");
    hit3.setSource("sounds/Booms5.ogg");
    return true;
}
