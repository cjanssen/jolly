// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PROGRESSBAR__H__
#define __PROGRESSBAR__H__

class ProgressBarPrivate;
class ProgressBar {
public:
    ProgressBar();
    ~ProgressBar();
    void update(double dt);
    virtual void draw() = 0;

    enum Direction {
        UpToDown,
        DownToUp,
        LeftToRight,
        RightToLeft
    };

    void setMax(double max);
    void setMin(double min);
    void setCurrent(double current);
    double current() const;
    void forceCurrent(double current);
    void setFixedSpeed(double speed); // units per second
    void setFixedDelay(double delay); // seconds per change
    void setGeometry(int x, int y, int w, int h);
    void setDirection(Direction dir);

protected:
    ProgressBarPrivate *d;
};

class DrawableProgressBar : public ProgressBar {
public:
    DrawableProgressBar();
    ~DrawableProgressBar();

    virtual void draw();

public:
    int borderWidth;
};

#endif
