TEMPLATE = app
CONFIG += console
CONFIG -= qt

#INCLUDEPATH += /usr/include/SDL
#QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage

LIBS += -lSDL -lSDL_ttf -lSDL_mixer -lSDL_image
#-lSDL_gfx

SOURCES += main.cpp \
mymath.cpp \
audio.cpp \
game.cpp \
mainloop.cpp \
movement.cpp \
player.cpp \
sprites.cpp \
keys.cpp \
text.cpp \
powerupmanager.cpp \
powerups.cpp \
bullets.cpp \
imageresources.cpp \
progressbar.cpp \
timers.cpp \
gamestate.cpp \
scenes.cpp \
entities.cpp \
gamescene.cpp \
globals.cpp

HEADERS += mymath.h\
audio.h \
game.h \
mainloop.h \
movement.h \
player.h \
sprites.h \
keys.h \
text.h \
powerupmanager.h \
powerups.h \
bullets.h \
imageresources.h \
progressbar.h \
timers.h \
gamestate.h \
scenes.h \
entities.h \
gamescene.h \
globals.h

RESOURCES +=

FORMS +=


