// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __AUDIO__H__
#define __AUDIO__H__

class SoundClipPrivate;
class MusicClipPrivate;

class MixerInstance
{
public:
    explicit MixerInstance();
    ~MixerInstance();
    bool isValid() const;
private:
    bool valid;
};

class SoundClip
{
public:
    explicit SoundClip();
    ~SoundClip();

    const char *source() const;
    void setSource(const char *newSource);
    bool playing() const;
    int volume() const;
    void setVolume(int newVolume);

    void play();
    void stop();

private:
    SoundClipPrivate *d;
};

class MusicClip
{
    public:
    explicit MusicClip();
    ~MusicClip();

    const char *source() const;
    void setSource(const char *newSource);
    bool playing() const;
    int loops() const;
    bool repeating() const;
    void setLoops(int loops);
    int fadeInTime() const;
    void setFadeInTime(int ms);
    void setRepeating(bool repeating);
    int volume() const;
    void setVolume(int newVolume);

    // used internally
    void notifyFinish();

    void play();
    void stop();
    void enqueue();
    void unqueue();
    void fadeOut(int ms);

private:
    MusicClipPrivate *d;
};

#endif //__AUDIO__H__
