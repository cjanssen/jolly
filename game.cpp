// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"
#include <stdio.h>

#include "SDL/SDL.h"

#include "imageresources.h"

#include "globals.h"


// "Level" manager
// - start game: Show title + instructions
// - enter players: ask to press key long to enter
// - in-game, with lives and explosions
// - player loses: other player gets 1 victory point, waiting for next player


GameLoop::GameLoop()
{
}

GameLoop::~GameLoop()
{
    Globals::instance()->finish();
}


bool GameLoop::load()
{
    if (!MainLoop::load())
        return false;

    if (!ImageSpace::instance->load())
        return false;

    return Globals::instance()->load();
}

void GameLoop::draw()
{
    Globals::instance()->sceneManager.draw();

    MainLoop::draw();
}

void GameLoop::update(double dt)
{
    Globals::instance()->sceneManager.update(dt);
}

void GameLoop::onKeyPressed(int key)
{
    Globals::instance()->keyboardManager.managePressed(key);

    SDLKey keyCode = (SDLKey)key;
    if (keyCode == SDLK_F11)
        toggleFPS();

    if (keyCode == SDLK_ESCAPE)
        requestExit();
}

void GameLoop::onKeyReleased(int key)
{
    Globals::instance()->keyboardManager.manageReleased(key);
}

void GameLoop::onMouseMove(int x, int y)
{
}

#define MOUSE_LEFT 1
#define MOUSE_RIGHT 3
#define MOUSE_MIDDLE 2
#define MOUSE_WHEELUP 4
#define MOUSE_WHEELDOWN 5

void GameLoop::onMousePressed(int x, int y, int button)
{

}
void GameLoop::onMouseReleased(int x, int y, int button)
{

}

