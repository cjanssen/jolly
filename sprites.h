// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SPRITES__H__
#define __SPRITES__H__

struct SDL_Surface;
struct _TTF_Font;
struct SDL_Color;

struct Color {
    int r,g,b;
    Color():r(0),g(0),b(0){}
    Color(int _r,int _g,int _b):r(_r),g(_g),b(_b){}
    const SDL_Color toSDL() const;
    bool operator==(const Color &c2) const {
        return r==c2.r && g==c2.g && b==c2.b;
    }
    bool operator!=(const Color &c2) const {
        return !(c2==*this);
    }
};

class Colors {
public:
    static Color green;
    static Color dk_green;
    static Color red;
    static Color black;
    static Color orange;
    static Color dk_red;
    static Color lt_red;
    static Color dark_black;
    static Color lt_blue;
    static Color yellow;

    static Color background;
    static Color foreground;
};

class Drawable {
public:
    Drawable() {}
    ~Drawable() {}
    virtual void draw(int x, int y, SDL_Surface *destSurf = 0, bool centered = true) = 0;
    virtual void reset() = 0;
    virtual void update(double dt) = 0;
    virtual int width() const = 0;
    virtual int height() const = 0;
    virtual int length() const = 0;
    virtual void forceFrame(int n) = 0;
};

class Image : public Drawable {
public:
    Image(const char *_filename = 0);
    ~Image();
    virtual int width() const;
    virtual int height() const;
    virtual int length() const;
    void load(const char *_filename = 0);
    void draw(int x, int y, SDL_Surface *destSurf = 0, bool centered = true);
    virtual void reset() {}
    virtual void update( double dt ) {}
    virtual void forceFrame(int n) {}


    // cutting out sections
    void section(const char *_filename, int x, int y, int w, int h);
    void section(const char *_filename, double x_fraction, double y_fraction, double w_fraction, double h_fraction);
    void section(const Image &refImage, int x, int y, int w, int h);

private:
    char filename[256];
    SDL_Surface *data;
};

class AnimationInstance;
class Animation : public Drawable {
public:
    Animation();
    ~Animation();
    Animation(const char *filename, int _w, int _h, double _frametime, bool _looping = true );
    Animation(const Image &_strip, int _w, int _h, double _frametime, bool _looping = true );

    virtual int width() const;
    virtual int height() const;
    virtual int length() const;
    void init(const Image &_strip, int _w, int _h, double _frametime, bool _looping = true);
    virtual void reset();
    virtual void update( double dt );
    virtual void draw(int x, int y, SDL_Surface *destSurf = 0, bool centered = true);
    virtual void forceFrame(int n);

private:
    int frame_w;
    int frame_h;
    int frame_count;
    int frame_ndx;
    double frame_delay;
    double frame_timer;
    Image *strip;
    bool looping;

    friend class AnimationInstance;
};


class AnimationInstance : public Drawable
{
public:
    AnimationInstance(Animation *animation = 0);
    ~AnimationInstance();
    void fromAnimation(Animation *animation);
    virtual int width() const;
    virtual int height() const;
    virtual int length() const;
    virtual void reset();
    virtual void update( double dt );
    virtual void draw(int x, int y, SDL_Surface *destSurf = 0, bool centered = true);
    virtual void forceFrame(int n);

private:
    int frame_ndx;
    double frame_timer;
    Animation *referenceAnim;

};

#endif //__SPRITES__H__
