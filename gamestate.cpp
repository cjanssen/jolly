// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#include "gamestate.h"

#include <vector>

class StateManagerPrivate {
public:
    StateManagerPrivate(StateManager *qq):q(qq){}
    StateManager *q;

    std::vector<State *>stateList;
    int currentState;
};

StateManager::StateManager() : d(new StateManagerPrivate(this))
{
    d->currentState = -1;
}
StateManager::~StateManager(){
    delete d;
}

int StateManager::append(State *newState)
{
    int newId = d->stateList.size();
    d->stateList.push_back(newState);
    return newId;
}

void StateManager::switchTo(int id)
{
    if (d->currentState >= 0 && d->currentState < d->stateList.size())
        d->stateList[d->currentState]->disable();

    if (id >= 0 && id < d->stateList.size()) {
        d->currentState = id;
        d->stateList[d->currentState]->enable();
    }
}

State *StateManager::at(int id) const
{
    return d->stateList[id];
}

void StateManager::next()
{
    // by now make it just "next one in row"
    switchTo(d->currentState+1);
}

int StateManager::current() const
{
    return d->currentState;
}

State *StateManager::currentState() const
{
    // no checks, just crash if invalid
    return d->stateList[d->currentState];
}

bool StateManager::validState() const
{
    return (d->currentState >= 0 && d->currentState < d->stateList.size());
}

void StateManager::clear()
{
    for (std::vector<State *>::iterator i = d->stateList.begin();
         i != d->stateList.end();
         ++i)
        delete (*i);
    d->stateList.clear();
}
