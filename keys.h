// JollyJoy
// Copyright 2011-2013 Christiaan Janssen, June 2012-April 2013
//
// This file is part of JollyJoy.
//
//     JollyJoy is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     JollyJoy is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with JollyJoy.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __KEYS__H__
#define __KEYS__H__

#include <list>

class KeyboardManager {
public:
    typedef int ActionCode;

    KeyboardManager();
    ~KeyboardManager();

    int count() const;

    bool load();
    void reset();
    void setKey(const ActionCode action, const int keyCode);

    bool isPressed(const ActionCode action) const;
    bool consumePressed(const ActionCode action);
    bool noKey() const;
    std::list<int> codeForAction(const ActionCode action) const;
    ActionCode actionForCode(const int keyCode) const;

    void managePressed(const int keyCode);
    void manageReleased(const int keyCode);

    int newAction(const int keyCode);
    void addKey(const ActionCode action, const int keyCode);
    void removeKey(const int keyCode);
    void cleanAction(const ActionCode action);

    bool *actionBinding(const KeyboardManager::ActionCode action) const;
private:
    class KeysPrivate;
    KeysPrivate *d;
};



#endif //__KEYS__H__
